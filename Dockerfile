FROM rust:latest

ENV APP_NAME test-ssh
ENV APP_HOME /app/$APP_NAME
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME

COPY . .

RUN cargo install --path .
EXPOSE 3000