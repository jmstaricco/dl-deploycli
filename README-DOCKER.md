# Ubuntu rust image

## Prerequisites

Have `docker` installed.

## How to run

First build the image if it is the first time

    docker build . -t test-ssh

To run the image just use the following

    docker run -it --rm --entrypoint "/bin/bash" --name test-ssh --mount type=bind,source="$(pwd)"/src,target=/app/test-ssh/src test-ssh:latest

To run the image with port visible

    docker run -it --rm --entrypoint "/bin/bash" --name test-ssh -p 3000:3000 --mount type=bind,source="$(pwd)"/src,target=/app/test-ssh/src test-ssh:latest

You should see this output

    root@8b8df78caab8:/app/test-ssh#

That means you're inside the docker container.

Try running `cargo run` to test if everything works correctly.

## Useful info

We're using a `bind mount` of the `src` folder. This means that all changes locally will reflect in the container so local development is possible.

Stops the container.

`docker stop test-ssh`

Shows the container logs.

`docker logs test-ssh`

Removes the container.

`docker rm test-ssh`

Removes the container image. If you run this you will have to re run the `build` command.

`docker rmi test-ssh`
