CLI for server depls.

## Prerequisites

Have `rust` installed. ?

## How to install
cargo build is accepting a —release flag so that we can specify that we want the final version of our executable:

    cargo build --release

The generated executable is located in a sub-directory: ./target/release/kt.

Either you copy-paste this file somewhere in your PATH, or you use another cargo command to install it automatically. The application will be installed in the ~/.cargo/bin/ directory (make sure this directory is in your PATH in the ~/.bashrc or ~/.zshrc).

    cargo install --path .

## How to run 
    $ dk -V
    myapp_clap 1.0
