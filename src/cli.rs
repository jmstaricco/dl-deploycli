use std::io;

use async_ssh2_lite::AsyncSession;

use futures::AsyncReadExt;

use std::fs::File;

pub async fn run_com_and_print(
    session: AsyncSession<std::net::TcpStream>,
    command: &str,
) -> io::Result<()> {
    let mut channel = session.channel_session().await?;
    channel.exec(command).await?;
    let mut s = String::new();
    channel.read_to_string(&mut s).await?;
    // println!("{}: {}", command, s);
    println!("{}", s);

    channel.close().await?;
    // println!("channel exit_status: {}", channel.exit_status()?);

    Ok(())
}

pub fn get_data(cfile: bool, f: &str) -> (String, String, String, Option<String>) {
    // if a config file is selected with the c arg, we have to get the data from that file
    let mut file = File::open("C:\\Users\\Usuario\\Desktop\\dl-deploycli\\config.json").expect("file should open read only");
    if cfile {
        println!("Charging new config file: {}", f);
        file = File::open(f).expect("file should open read only"); 
    }
    let json: serde_json::Value =
        serde_json::from_reader(file).expect("JSON was not well-formatted");
    // load the config.json, de esta forma el config o environement variables?
    let mut addr = json["server_address"].to_string().to_owned();
    addr.retain(|c| c != '"');
    let mut username = json.get("username").unwrap().to_string().to_owned();
    username.retain(|c| c != '"');
    let mut privatekey = json.get("key_path").unwrap().to_string().to_owned();
    privatekey.retain(|c| c != '"');
    let mut pre_passphrase = json.get("passphrase").unwrap().to_string().to_owned();
    pre_passphrase.retain(|c| c != '"');
    let passphrase = Some(pre_passphrase);
    (addr, username, privatekey, passphrase)
}

pub fn args_clean(vec: &mut Vec<String>) {
    vec.remove(0);
    if vec.contains(&"-c".to_string()){
        let index = vec.iter().position(|r| r == "-c").unwrap();
        vec.remove(index);  // remove -c
        vec.remove(index); // remove config file
    } else if vec.contains(&"--config".to_string()){
        let index = vec.iter().position(|r| r == "config").unwrap();
        vec.remove(index);  // remove -c
        vec.remove(index); // remove config file
    }
}