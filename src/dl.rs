/*
cargo run -p test-ssh --bin dl

cargo build -p test-ssh --bin dl

./target/debug/dl
*/

use clap::{load_yaml, App, AppSettings, Arg};

// auth imports
use std::env;
use std::io;
use std::net::{TcpStream, ToSocketAddrs};
use std::path::Path;

use async_io::Async;
use futures::executor::block_on;

use async_ssh2_lite::AsyncSession;

// run commands imports
use futures::AsyncReadExt;

// cli imports
mod cli;

use serde_json::*;


fn main() -> io::Result<()> {
    block_on(run())
}

async fn run() -> io::Result<()> {
    // TODO: Be able to charge the matches with the yaml file
    // NOTE: Not all settings may be set using the usage string method. Some properties are only available via the builder pattern.
    // let yaml = load_yaml!("app_settings.yaml");
    // let matches = App::from(yaml)
        // .get_matches();

    let matches = App::new("MyApp")
    .version("1.0")
    .author("Juan Martin Staricco <juanmartinstaricco@gmail.com>")
    .about("Does awesome things")
    .arg("-c, --config=[FILE] 'Sets a custom config file'")
    .arg(Arg::new("cmd").required(false))
    .arg("-d..., --debug... 'Turn debugging information on'")
    .arg("-t 'Sort by time, newest first'")
    .arg("-x 'list entries by lines instead of by columns'")
    .arg("-l, --list 'lists more detailed values'")
    .subcommand(
        App::new("mkdir")
            .about("Creates a new directory")
            .arg(
                Arg::new("input")
                    .short('i')
                    .long("input")
                    .takes_value(true)
                    .multiple(true),
                ),
    )
    .get_matches();

    let mut cfile = false;
    if let Some(c) = matches.value_of("config") {
        println!("Value for config: {}", c);
        cfile = true;
    }

    // Start server session and auth
    let (addr, username, privatekey, passphrase) = cli::get_data(cfile, matches.value_of("config").unwrap_or(""));
    // println!("{}", addr);
    // println!("{}", username);
    // println!("{}", privatekey);
    // println!("{:?}", passphrase);

    // initialize variables for the server  access

    let addr = addr.to_socket_addrs().unwrap().next().unwrap();

    let stream = Async::<TcpStream>::connect(addr).await?;

    let mut session = AsyncSession::new(stream, None)?;

    session.handshake().await?;

    session
        .userauth_pubkey_file(
            username.as_ref(),
            None,
            Path::new(&privatekey),
            passphrase.as_deref(),
        )
        .await?;

    if !session.authenticated() {
        return Err(session
            .last_error()
            .and_then(|err| Some(io::Error::from(err)))
            .unwrap_or(io::Error::new(
                io::ErrorKind::Other,
                "unknown userauth error",
            )));
    }

    println!("Connected to the server!");
    println!("Entering the command running phase in the server...");
    println!("");

    if let Some(clone_matches) = matches.subcommand_matches("mkdir") {
        let new_dirs = clone_matches
            .values_of("input")
            .unwrap()
            .collect::<Vec<_>>();

        for i in new_dirs.iter() {
            println!("Creating directory named {}", i);
            // cli::run_com_and_print(session, "mkdir " + &i).await?;
        }
    }


    // println!("{:?}", matches);
    if matches.subcommand().is_none() {
        let mut args: Vec<String> = env::args().collect();
        cli::args_clean(&mut args);
        let command = args.join(" ");
        cli::run_com_and_print(session, &command).await?;
    }
    Ok(())
}