/*
cargo run -p test-ssh --bin main addr username pubkeypath passphrase
cargo run -p test-ssh --bin main 104.131.63.13:22 root C:\Users\Usuario/.ssh/id_rsa december12

cargo run -p test-ssh --bin main 104.131.63.13:22 root .ssh/id_rsa december12
*/

// auth imports
use std::env;
use std::io;
use std::net::{TcpStream, ToSocketAddrs};
use std::path::Path;

use async_io::Async;
use futures::executor::block_on;

use async_ssh2_lite::AsyncSession;

// run commands imports
use futures::AsyncReadExt;

// cli imports
mod cli;

fn main() -> io::Result<()> {
    block_on(run())
}

async fn run() -> io::Result<()> {
    // Connection to the server example
    //  
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| env::var("ADDR").unwrap_or_else(|_| "104.131.63.13:22".to_owned()));
    let username = env::args()
        .nth(2)
        .unwrap_or_else(|| env::var("USERNAME").unwrap_or_else(|_| "root".to_owned()));
    let privatekey = env::args()
        .nth(3)
        .unwrap_or_else(|| env::var("PRIVATEKEY").unwrap_or_else(|_| ".ssh/id_rsa".to_owned()));
    // let passphrase = env::args().nth(4).unwrap_or_else(|| env::var("PASSPHRASE").unwrap_or_else(|_| "december12".to_owned()));
    let passphrase = env::args().nth(3).or_else(|| env::var("PASSPHRASE").ok());

    println!("{}", addr);
    println!("{}", privatekey);
    println!("{:?}", passphrase);

    let addr = addr.to_socket_addrs().unwrap().next().unwrap();

    let stream = Async::<TcpStream>::connect(addr).await?;

    let mut session = AsyncSession::new(stream, None)?;

    session.handshake().await?;

    session
        .userauth_pubkey_file(
            username.as_ref(),
            None,
            Path::new(&privatekey),
            passphrase.as_deref(),
        )
        .await?;

    if !session.authenticated() {
        return Err(session
            .last_error()
            .and_then(|err| Some(io::Error::from(err)))
            .unwrap_or(io::Error::new(
                io::ErrorKind::Other,
                "unknown userauth error",
            )));
    }

    // Cli::new(session).run().await?;

    Ok(())
}
